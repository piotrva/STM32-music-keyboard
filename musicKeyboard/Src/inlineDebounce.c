# include "inlineDebounce.h"

bool debounce(debouncer_t* debouncer, bool actualState, uint32_t actualTicks){
	bool toReturn = false;
	if(actualState){
		switch(debouncer->state){
		case IDLE:
		default:
			debouncer->state = DEBOUNCE;
			debouncer->lastTime = actualTicks;
			break;
		case DEBOUNCE:
			if((actualTicks - debouncer->lastTime) >= debouncer->clickMin){
				toReturn = true;
				debouncer->state = HOLD;
			}
			break;
		case HOLD:
			debouncer->lastTime = actualTicks;
			break;
		}
	}else{ // actualState == false;
		switch(debouncer->state){
			case IDLE:
			default:
				break;
			case DEBOUNCE:
				debouncer->state = IDLE;
				break;
			case HOLD:
				if((actualTicks - debouncer->lastTime) >= debouncer->clickMin){
					debouncer->state = IDLE;
				}
				break;
			}
	}

	return toReturn;
}

bool debounceRep(debouncer_t* debouncer, bool actualState, uint32_t actualTicks){
	bool toReturn = false;
	if(actualState){
		switch(debouncer->state){
		case IDLE:
		default:
			debouncer->state = DEBOUNCE;
			debouncer->lastTime = actualTicks;
			break;
		case DEBOUNCE:
			if((actualTicks - debouncer->lastTime) >= debouncer->clickMin){
				toReturn = true;
				debouncer->state = HOLD_WAIT;
				debouncer->lastHold = actualTicks;
			}
			break;
		case HOLD_WAIT:
			debouncer->lastTime = actualTicks;
			if((actualTicks - debouncer->lastHold) >= debouncer->holdWait){
				toReturn = true;
				debouncer->lastHold = actualTicks;
				debouncer->state = HOLD;
			}
			break;
		case HOLD:
			debouncer->lastTime = actualTicks;
			if((actualTicks - debouncer->lastHold) >= debouncer->holdRep){
				toReturn = true;
				debouncer->lastHold = actualTicks;
			}
			break;
		}
	}else{ // actualState == false;
		switch(debouncer->state){
			case IDLE:
			default:
				break;
			case DEBOUNCE:
				debouncer->state = IDLE;
				break;
			case HOLD:
			case HOLD_WAIT:
				if((actualTicks - debouncer->lastTime) >= debouncer->clickMin){
					debouncer->state = IDLE;
				}
				break;
			}
	}

	return toReturn;
}

bool debounceSimple(debouncer_t* debouncer, bool actualState, uint32_t actualTicks, bool* edge){
	if(edge != NULL){
		*edge = false;
	}
	if(actualState == debouncer->lastState){
		debouncer->state = IDLE;
	}else{ // actualState != debouncer->lastState;
		switch(debouncer->state){
			case IDLE:
			default:
				debouncer->state = DEBOUNCE;
				debouncer->lastTime = actualTicks;
				break;
			case DEBOUNCE:
				if((actualTicks - debouncer->lastTime) >= debouncer->clickMin){
					debouncer->state = IDLE;
					debouncer->lastState = actualState;
					if(edge != NULL){
						*edge = true;
					}
				}
				break;
		}
	}

	return debouncer->lastState;
}
