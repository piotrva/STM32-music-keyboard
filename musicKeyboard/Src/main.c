/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "iwdg.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "usbd_hid.h"
#include "keyboardCodes.h"
#include "inlineDebounce.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
#define ENC_PRESC 4
#define ENC_DEB 10
#define BTN_DEB 50
/* Private variables ---------------------------------------------------------*/
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void KeyboardWriteMusic(uint16_t code){
    static uint8_t buff[3] = {4, 0, 0}; /* 3 bytes long report */

    buff[1] = code;
    buff[2] = (code >> 8);

    USBD_HID_SendReport(&hUsbDeviceFS, buff, 3);
    HAL_Delay(5);

    buff[1] = 0;
    buff[2] = 0;
    USBD_HID_SendReport(&hUsbDeviceFS, buff, 3);
    HAL_Delay(5);
}

void KeyboardWrite(uint8_t special, uint8_t Key0, uint8_t Key1, uint8_t Key2, uint8_t Key3, uint8_t Key4, uint8_t Key5){
    static uint8_t buff[9] = {1, 0, 0, 0, 0, 0, 0, 0, 0}; /* 9 bytes long report */

    buff[1] = special;

    buff[2] = 0x00;

    buff[3] = Key0;
    buff[4] = Key1;
    buff[5] = Key2;
    buff[6] = Key3;
    buff[7] = Key4;
    buff[8] = Key5;

    USBD_HID_SendReport(&hUsbDeviceFS, buff, 9);
    HAL_IWDG_Refresh(&hiwdg);
    HAL_Delay(5);

    for(int i = 1 ; i < 9 ; i++){
        buff[i] = 0x00;
    }

    USBD_HID_SendReport(&hUsbDeviceFS, buff, 9);
    HAL_IWDG_Refresh(&hiwdg);
    HAL_Delay(5);
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
void reenumerate(){
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GPIO_PIN_12;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//reenumerate
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
	HAL_Delay(500);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
	HAL_Delay(500);
}

volatile int encCounter[2] = {0, 0};
int encCounterLocal[2] = {0, 0};
debouncer_t enc1ADebouncer = {.clickMin = ENC_DEB};
debouncer_t enc1BDebouncer = {.clickMin = ENC_DEB};

debouncer_t enc2ADebouncer = {.clickMin = ENC_DEB};
debouncer_t enc2BDebouncer = {.clickMin = ENC_DEB};

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	static uint32_t us100Tick = 0;
	us100Tick ++;


	bool edgeA, edgeB;
	bool encA = debounceSimple(&enc1ADebouncer, HAL_GPIO_ReadPin(ENC1A_GPIO_Port, ENC1A_Pin), us100Tick, &edgeA);
	bool encB = debounceSimple(&enc1BDebouncer, HAL_GPIO_ReadPin(ENC1B_GPIO_Port, ENC1B_Pin), us100Tick, &edgeB);

	int dir = 0;

	if(edgeA && encA){
		if(encB == false){
			dir = 1;
		}else{
			dir = -1;
		}
	}else if(edgeA && !encA){
		if(encB == false){
			dir = -1;
		}else{
			dir = 1;
		}
	}else if(edgeB && encB){
		if(encA == false){
			dir = -1;
		}else{
			dir = 1;
		}
	}else if(edgeB && !encB){
		if(encA == false){
			dir = 1;
		}else{
			dir = -1;
		}
	}

	if(dir == 1){
		encCounter[0]++;
	}else if(dir == -1){
		encCounter[0]--;
	}

	dir = 0;

	encA = debounceSimple(&enc2ADebouncer, HAL_GPIO_ReadPin(ENC2A_GPIO_Port, ENC2A_Pin), us100Tick, &edgeA);
	encB = debounceSimple(&enc2BDebouncer, HAL_GPIO_ReadPin(ENC2B_GPIO_Port, ENC2B_Pin), us100Tick, &edgeB);

	if(edgeA && encA){
		if(encB == false){
			dir = 1;
		}else{
			dir = -1;
		}
	}else if(edgeA && !encA){
		if(encB == false){
			dir = -1;
		}else{
			dir = 1;
		}
	}else if(edgeB && encB){
		if(encA == false){
			dir = -1;
		}else{
			dir = 1;
		}
	}else if(edgeB && !encB){
		if(encA == false){
			dir = 1;
		}else{
			dir = -1;
		}
	}

	if(dir == 1){
		encCounter[1]++;
	}else if(dir == -1){
		encCounter[1]--;
	}

}

debouncer_t BTN0Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN1Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN2Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN3Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN4Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN5Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN6Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN7Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN8Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN9Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN10Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN11Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN12Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN13Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN14Debouncer = {.clickMin = BTN_DEB};
debouncer_t BTN15Debouncer = {.clickMin = BTN_DEB};

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  //MX_USB_DEVICE_Init();
  MX_IWDG_Init();
  MX_USART1_UART_Init();
  MX_TIM1_Init();

  /* USER CODE BEGIN 2 */
  reenumerate();
  MX_USB_DEVICE_Init();
  HAL_TIM_Base_Start_IT(&htim1);
  HAL_IWDG_Start(&hiwdg);
  // initialize timers for encoders

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
	  HAL_IWDG_Refresh(&hiwdg);
	  __disable_irq();
	  encCounterLocal[0] += encCounter[0];
	  encCounter[0] = 0;

	  encCounterLocal[1] += encCounter[1];
	  encCounter[1] = 0;
	  __enable_irq();
	  HAL_IWDG_Refresh(&hiwdg);


	  if(encCounterLocal[0] > 0){
		  int absolute = encCounterLocal[0] / ENC_PRESC;
		  encCounterLocal[0] %= ENC_PRESC;
		  for(int i = 0 ; i < absolute ; i++){
			  //action for encoder 1 +
			  KeyboardWriteMusic(0x00E9); // vol+
		  }
	  }else if(encCounterLocal[0] < 0){
		  int absolute = (encCounterLocal[0] * -1) / ENC_PRESC;
		  encCounterLocal[0] = ((encCounterLocal[0] * -1) % ENC_PRESC) * -1;
		  for(int i = 0 ; i < absolute ; i++){
			  //action for encoder 1 -
			  KeyboardWriteMusic(0x00EA); // vol-
		  }
	  }

	  if(encCounterLocal[1] > 0){
		  int absolute = encCounterLocal[1] / ENC_PRESC;
		  encCounterLocal[1] %= ENC_PRESC;
		  for(int i = 0 ; i < absolute ; i++){
			  //action for encoder 2 +
			  KeyboardWriteMusic(0x00E9);
		  }
	  }else if(encCounterLocal[1] < 0){
		  int absolute = (encCounterLocal[1] * -1) / ENC_PRESC;
		  encCounterLocal[1] = ((encCounterLocal[1] * -1) % ENC_PRESC) * -1;
		  for(int i = 0 ; i < absolute ; i++){
			  //action for encoder 2 -
			  KeyboardWriteMusic(0x00EA);
		  }
	  }

	  HAL_IWDG_Refresh(&hiwdg);
	  HAL_Delay(10);
	  HAL_IWDG_Refresh(&hiwdg);

	  if(debounce(&BTN0Debouncer, !HAL_GPIO_ReadPin(BTN0_GPIO_Port, BTN0_Pin),HAL_GetTick())){
		  //action for BTN0
		  KeyboardWriteMusic(0x00E2);  // mute
	  }
	  if(debounce(&BTN1Debouncer, !HAL_GPIO_ReadPin(BTN1_GPIO_Port, BTN1_Pin),HAL_GetTick())){
		  //action for BTN1
		  KeyboardWrite(SHIFT_LEFT, KEY_X, KEY_D, 0x00, 0, 0, 0);  // [Shift][X][D]
		  KeyboardWrite(0x00, KEY_RETURN, 0x00, 0x00, 0x00, 0, 0);  // [Enter]
	  }
	  if(debounce(&BTN2Debouncer, !HAL_GPIO_ReadPin(BTN2_GPIO_Port, BTN2_Pin),HAL_GetTick())){
		  //action for BTN2
		  KeyboardWrite(SHIFT_LEFT, KEY_X, KEY_D, 0x00, 0x00, 0x00, 0x00);  // [Shift][X][D]
		  for(int i = 0 ; i < 83 ; i++){
			  KeyboardWrite(SHIFT_LEFT, KEY_D, 0x00, 0x00, 0x00, 0x00, 0x00);  // [Shift][D]
		  }
		  KeyboardWrite(0x00, KEY_RETURN, 0x00, 0x00, 0x00, 0, 0);  // [Enter]
	  }
	  if(debounce(&BTN3Debouncer, !HAL_GPIO_ReadPin(BTN3_GPIO_Port, BTN3_Pin),HAL_GetTick())){
	    //action for BTN3
	  }
	  if(debounce(&BTN4Debouncer, !HAL_GPIO_ReadPin(BTN4_GPIO_Port, BTN4_Pin),HAL_GetTick())){
	    //action for BTN4
	  }
	  if(debounce(&BTN5Debouncer, !HAL_GPIO_ReadPin(BTN5_GPIO_Port, BTN5_Pin),HAL_GetTick())){
	    //action for BTN5
	  }
	  if(debounce(&BTN6Debouncer, !HAL_GPIO_ReadPin(BTN6_GPIO_Port, BTN6_Pin),HAL_GetTick())){
	    //action for BTN6
	  }
	  if(debounce(&BTN7Debouncer, !HAL_GPIO_ReadPin(BTN7_GPIO_Port, BTN7_Pin),HAL_GetTick())){
	    //action for BTN7
	  }
	  if(debounce(&BTN8Debouncer, !HAL_GPIO_ReadPin(BTN8_GPIO_Port, BTN8_Pin),HAL_GetTick())){
	    //action for BTN8
	  }
	  if(debounce(&BTN9Debouncer, !HAL_GPIO_ReadPin(BTN9_GPIO_Port, BTN9_Pin),HAL_GetTick())){
	    //action for BTN9
	  }
	  if(debounce(&BTN10Debouncer, !HAL_GPIO_ReadPin(BTN10_GPIO_Port, BTN10_Pin),HAL_GetTick())){
	    //action for BTN10
	  }
	  if(debounce(&BTN11Debouncer, !HAL_GPIO_ReadPin(BTN11_GPIO_Port, BTN11_Pin),HAL_GetTick())){
	    //action for BTN11
	  }
	  if(debounce(&BTN12Debouncer, !HAL_GPIO_ReadPin(BTN12_GPIO_Port, BTN12_Pin),HAL_GetTick())){
	    //action for BTN12
	  }
	  if(debounce(&BTN13Debouncer, !HAL_GPIO_ReadPin(BTN13_GPIO_Port, BTN13_Pin),HAL_GetTick())){
	    //action for BTN13
	  }
	  if(debounce(&BTN14Debouncer, !HAL_GPIO_ReadPin(BTN14_GPIO_Port, BTN14_Pin),HAL_GetTick())){
	    //action for BTN14
	  }
	  if(debounce(&BTN15Debouncer, !HAL_GPIO_ReadPin(BTN15_GPIO_Port, BTN15_Pin),HAL_GetTick())){
	    //action for BTN15
	  }
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
